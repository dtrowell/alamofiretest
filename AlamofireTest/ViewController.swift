//
//  ViewController.swift
//  AlamofireTest
//
//  Created by Doctor Octopus on 6/21/17.
//  Copyright © 2017 ProObject. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    let DATABASE_DOMAIN = "http://208.109.53.180/runnerbuddy/"
    let DATABASE_USERLIST = "user/list"
    let DATABASE_OBSERVATION = "observation"
    
    @IBOutlet weak var userNameField: UITextField!
    @IBOutlet weak var uploadButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func uploadListener(_ sender: Any) {
        let 😋: String = userNameField.text!
        print(😋)
        //put to database
        let parameters: Parameters = ["user_name":😋]
        
        Alamofire.request(DATABASE_DOMAIN+DATABASE_OBSERVATION, method: .post, parameters: parameters, encoding: JSONEncoding.default).responseString {response in
            print("Request: \(String(describing: response.request))")
            print("Response: \(String(describing: response.response))")
            print("Result: \(String(describing: response.result))")
        }
    }

    @IBAction func getListener(_ sender: Any) {
        let url = DATABASE_DOMAIN + DATABASE_USERLIST
        Alamofire.request(url, method: .get).responseString { response in
            print("Request: \(String(describing: response.request))")
            print("Response: \(String(describing: response.response))")
            print("Result: \(String(describing: response.result))")
            print("Data: \(String(describing: response.result.value))")
        }
    }
}

